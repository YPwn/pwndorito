#include "MpNick.h"

#include "../ElDorito.h"
#include "../BlamTypes.h"
#include "../Utils/String.h"

#include <Windows.h>
#include <iostream>
#include <iostream>

MpNick::MpNick()
{
	// Level load patch
	//const uint8_t NOP[] = { 0x90, 0x90, 0x90, 0x90, 0x90 };
	//memcpy((uint8_t*)GetBasePointer() + 0x2D26DF, NOP, sizeof(NOP));
	//Patch::NopFill(Pointer::Base(0x2D26DF), 5);
}

MpNick::~MpNick()
{
}

std::string MpNick::Info(const std::string& Topic) const
{
	std::string Nick = (char*)(0x19AB738);
	Nick = std::string(Nick.c_str());
	std::string Info = "Current nick: " + Nick + "\nUsage: mpnick (nickname)\n";

	return Info;
}

std::string MpNick::Suggest(const std::vector<std::string>& Arguments) const
{
	return "";
}

void MpNick::Tick(const std::chrono::duration<double>& Delta)
{
}

bool MpNick::Run(const std::vector<std::string>& Args)
{
	if( Args.size() >= 2 )
	{
		std::cout << "Setting nick: " + Args[1] << std::endl;
		std::string NickName_temp = Args[1];

		if (NickName_temp.length() > 15) {
			std::cout << "ERROR: Nickname is too long (max 15 chars)!" << std::endl;
			return false;
		}

		std::wstring NickName = std::wstring(NickName_temp.begin(), NickName_temp.end());

		// Nick Name
		Pointer(0x19AB738).Write(NickName.c_str(), (NickName_temp.length() + 1) * 2);

		return true;
	}

	return false;
}