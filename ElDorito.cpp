#include "ElDorito.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>

#include <Windows.h>
#include <conio.h> // _getch()
#include <cctype> //isprint

#include <codecvt>
#include <cvt/wstring> // wstring_convert

#include "Utils/Utils.h"
#include "ElModules.h"

const char* Credits[] = {
	"Wunk",
	"stoker25 (emoose)",
	"pwnsrv"
};

size_t ElDorito::MainThreadID = 0;

void __fastcall UI_MenuUpdateHook(void* a1, int unused, int menuIdToLoad)//void* a2, int a3, int menuIdToLoad)
{
	bool shouldUpdate = false;
	
	shouldUpdate = *(DWORD*)((uint8_t*)a1 + 0x10) >= 0x1E;
	typedef void(__thiscall *UI_MenuUpdateFunc)(void* a1, int menuIdToLoad);// void* a2, int a3, int menuIdToLoad);
	UI_MenuUpdateFunc menuUpdate = (UI_MenuUpdateFunc)0xADF6E0;
	menuUpdate(a1, menuIdToLoad);// a2, a3, menuIdToLoad);

	if (shouldUpdate)
	{
		static uint8_t UIData[0x40];

		typedef void*(__thiscall * OpenUIDialogByIdFunc)(void* a1, unsigned int a2, int a3, int a4, int a5);

		OpenUIDialogByIdFunc openui = (OpenUIDialogByIdFunc)0xA92780;
		void* ret = openui(&UIData, menuIdToLoad, 0xFF, 4, 0x1000D);

		if (ret != 0)
		{
			typedef int(*ShowUIDialog)(void* a1);

			ShowUIDialog showui = (ShowUIDialog)0xA93450;
			showui(&UIData);
		}
	}
}

char* Network_GetIPStringFromInAddr(void* inaddr)
{
	static char ipAddrStr[64];
	memset(ipAddrStr, 0, 64);

	uint8_t ip3 = *(uint8_t*)inaddr;
	uint8_t ip2 = *((uint8_t*)inaddr + 1);
	uint8_t ip1 = *((uint8_t*)inaddr + 2);
	uint8_t ip0 = *((uint8_t*)inaddr + 3);

	uint16_t port = *(uint16_t*)((uint8_t*)inaddr + 0x10);
	uint16_t type = *(uint16_t*)((uint8_t*)inaddr + 0x12);

	sprintf_s(ipAddrStr, 64, "%hd.%hd.%hd.%hd:%hd (%hd)", ip0, ip1, ip2, ip3, port, type);

	return ipAddrStr;
}

char Network_XnAddrToInAddrHook(void* pxna, void* pxnkid, void* in_addr)
{
	uint32_t maxMachines = *(uint32_t*)(0x228E6D8);
	uint8_t* syslinkDataPtr = (uint8_t*)*(uint32_t*)(0x228E6D8 + 0x4);

	for (uint32_t i = 0; i < maxMachines; i++)
	{
		uint8_t* entryPtr = syslinkDataPtr;
		syslinkDataPtr += 0x164F8;
		uint8_t entryStatus = *entryPtr;
		if (entryStatus == 0)
			continue;

		uint8_t* xnkidPtr = entryPtr + 0x9E;
		uint8_t* xnaddrPtr = entryPtr + 0xAE;
		uint8_t* ipPtr = entryPtr + 0x170;

		if (memcmp(pxna, xnaddrPtr, 0x10) == 0 && memcmp(pxnkid, xnkidPtr, 0x10) == 0)
		{
			// in_addr struct:
			// 0x0 - 0x10: IP (first 4 bytes for IPv4, 0x10 for IPv6)
			// 0x10 - 0x12: Port number
			// 0x12 - 0x14: IP length (4 for IPv4, 0x10 for IPv6)

			memset(in_addr, 0, 0x14);
			memcpy(in_addr, ipPtr, 4);
			//*(uint8_t*)in_addr = 0; // last part of IP
			//*((uint8_t*)in_addr + 1) = 0;
			//*((uint8_t*)in_addr + 2) = 0;
			//*((uint8_t*)in_addr + 3) = 0; // first part of IP

			*(uint16_t*)((uint8_t*)in_addr + 0x10) = 11774;
			*(uint16_t*)((uint8_t*)in_addr + 0x12) = 4;

			return 1;
		}
	}

	typedef int(*pFuncAddr)(void*, void*, void*);
	pFuncAddr pFunc = (pFuncAddr)(0x52D840);
	return pFunc(pxna, pxnkid, in_addr);
}

char Network_InAddrToXnAddrHook(void* ina, void* pxna, void* pxnkid)
{
	uint32_t maxMachines = *(uint32_t*)(0x228E6D8);
	uint8_t* syslinkDataPtr = (uint8_t*)*(uint32_t*)(0x228E6D8 + 0x4);

	for (uint32_t i = 0; i < maxMachines; i++)
	{
		uint8_t* entryPtr = syslinkDataPtr;
		syslinkDataPtr += 0x164F8;
		uint8_t entryStatus = *entryPtr;
		if (entryStatus == 0)
			continue;

		uint8_t* xnkidPtr = entryPtr + 0x9E; //2
		uint8_t* xnaddrPtr = entryPtr + 0xAE; //1
		uint8_t* ipPtr = entryPtr + 0x170; //3

		if (memcmp(ipPtr, ina, 4) == 0)
		{
			memcpy(pxna, xnaddrPtr, 0x10);
			memcpy(pxnkid, xnkidPtr, 0x10);

			return 1;
		}
	}

	typedef char(*pFuncAddr)(void*, void*, void*);
	pFuncAddr pFunc = (pFuncAddr)(0x52D840);
	return pFunc(ina, pxna, pxnkid);
}

ElDorito::ElDorito()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleOutputCP(437);
	unsigned int ConsoleWidth = 80;
	CONSOLE_SCREEN_BUFFER_INFO ConsoleBuf;
	if( GetConsoleScreenBufferInfo(hStdout, &ConsoleBuf) )
	{
		ConsoleWidth = ConsoleBuf.dwSize.X;
	}

	SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);

#ifdef _ELDEBUG
	std::string buildType = "Debug ";
#else
	std::string buildType = "Release ";
#endif
	std::cout << "ElDorito" << "\xC3\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xB4 " << buildType << "Build Date: " << __DATE__ << " @ " << __TIME__ << std::endl;
	Terminal.SetTextColor(Console::Input);

	std::srand(GetTickCount());
	for( size_t i = 0; i < sizeof(Credits) / sizeof(char*); i++ )
	{
		for( size_t k = 0; k < 8; k++ )
		{
			Terminal.SetTextColor(
				(std::rand() & 1 ? Console::Red : Console::Green) |
				std::rand() & 1 * Console::Bright
				);
			std::cout << " \x10 \x11 \x1E \x1E "[std::rand() & 7];
		}
		Terminal.SetTextColor(Console::Input);
		std::cout << "  " << "\xC3\xC0"[(i == sizeof(Credits) / sizeof(char*) - 1) ^ 0];
		std::cout << Credits[i] << std::endl;;
	}

	Terminal.SetTextColor(Console::Green | Console::Bright);
	std::cout << "Enter \"";
	Terminal.SetTextColor(Console::Input);
	std::cout << "help";
	Terminal.SetTextColor(Console::Green | Console::Bright);
	std::cout << "\" or \"";
	Terminal.SetTextColor(Console::Input);
	std::cout << "help (command)";
	Terminal.SetTextColor(Console::Green | Console::Bright);

	std::cout << "\" to get started!" << std::endl;
	std::cout << "Current directory: ";
	Terminal.SetTextColor(Console::Input);
	std::cout << GetDirectory() << std::endl;
	Terminal.SetTextColor(Console::Red | Console::Blue);
	std::cout << std::string(ConsoleWidth - 1, '\xC4') << std::endl;
	Terminal.SetTextColor(Console::Info);

	::CreateDirectoryA(GetDirectory().c_str(), NULL);

	// Enable write to all executable memory
	size_t Offset, Total;
	Offset = Total = 0;
	MEMORY_BASIC_INFORMATION MemInfo;

	//printf("\nUnprotecting memory...");
	while( VirtualQuery((uint8_t*)GetBasePointer() + Offset, &MemInfo, sizeof(MEMORY_BASIC_INFORMATION)) )
	{
		Offset += MemInfo.RegionSize;
		if( MemInfo.Protect == PAGE_EXECUTE_READ )
		{
			//printf("%0X\n", (size_t)((uint8_t*)GetBasePointer() + Offset));
			Total += MemInfo.RegionSize;
			VirtualProtect(MemInfo.BaseAddress, MemInfo.RegionSize, PAGE_EXECUTE_READWRITE, &MemInfo.Protect);
		}
	}
	//printf("\nDone! Unprotected %u bytes of memory\n", Total);

	// English patch
	Patch(0x2333FD, { 0 }).Apply();

	// Enable tag edits
	Patch(0x101A5B, { 0xEB }).Apply();
	Patch::NopFill(Pointer::Base(0x102874), 2);

	// No --account args patch
	Patch(0x43731A, { 0xEB, 0x0E }).Apply();
	Patch(0x4373AD, { 0xEB, 0x03 }).Apply();

	// Fix gamepad option in settings (todo: find out why it doesn't detect gamepads
	// and find a way to at least enable pressing ESC when gamepad is enabled)
	Patch::NopFill(Pointer::Base(0x20D7F2), 2);

	// Fix "Network" setting in lobbies (change broken 0x100B7 menuID to 0x100B6)
	Patch(0x6C34B0, { 0xB6 }).Apply();

	// Fix UI elements automatically closing on mainmenu-loaded maps (todo: find real reason why they close)
	//Patch(0x6AA870, { 0xC3 }).Apply();

	// Fix controller not working on UI elements when in mainmenu-loaded maps (todo: find why this patch works)
	//Patch(0x6AB33D, { 0xB8, 0x00, 0x00, 0x00, 0x00 }).Apply();
	//Patch(0x6B538F, { 0xB8, 0x00, 0x00, 0x00, 0x00 }).Apply();

	Hook(0x3F6F0, false, &Network_GetIPStringFromInAddr).Apply();
	Hook(0x6DFB73, true, &UI_MenuUpdateHook).Apply();
	Patch::NopFill(Pointer::Base(0x569D07), 3); // hacky fix
	
	Hook(0x30B6C, true, &Network_XnAddrToInAddrHook).Apply();
	Hook(0x30F51, true, &Network_InAddrToXnAddrHook).Apply();

	// Update window title patch
	const uint8_t windowData[] = { 0x3A, 0x20, 0x45, 0x6C, 0x20, 0x44, 0x6F, 0x72, 0x69, 0x74, 0x6F, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0x00 };
	Pointer::Base(0x159C02F).Write(windowData, sizeof(windowData));
	Pointer::Base(0x159C06F).Write(windowData, sizeof(windowData));
	Pointer::Base(0x1EAAEE0).Write(windowData, sizeof(windowData));

	// Register Modules
	PushModule<Test>("test");
	PushModule<Ammo>("ammo");
	PushModule<Camera>("camera");
	PushModule<DebugLogging>("debug");
	PushModule<Fov>("fov");
	PushModule<Globals>("global");
	PushModule<Godmode>("god");
	PushModule<Hud>("hud");
	PushModule<LoadLevel>("load");
	PushModule<MpNick>("mpnick");
	PushModule<Spawn>("spawn");
	PushModule<Information>("info");
	PushModule<ShowGameUI>("show_ui");
	//PushModule<ThirdPerson>("third");

	SetSessionMessage("ElDorito | Version: " + buildType + Utils::Version::GetInfo("File Version") + " | Build Date: " __DATE__);

	// Parse command-line commands
	int numArgs = 0;
	LPWSTR* szArgList = CommandLineToArgvW(GetCommandLineW(), &numArgs);
	if( szArgList && numArgs > 1 )
	{
		std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;

		for( int i = 1; i < numArgs; i++ )
		{
			std::wstring arg = std::wstring(szArgList[i]);
			if( arg.compare(0, 1, L"-") != 0 ) // if it doesn't start with -
				continue;

			size_t pos = arg.find(L"=");
			if( pos == std::wstring::npos || arg.length() <= pos + 1 ) // if it doesn't contain an =, or there's nothing after the =
				continue;

			std::string argname = converter.to_bytes(arg.substr(1, pos - 1));
			std::string argvalue = converter.to_bytes(arg.substr(pos + 1));

			if( Commands.count(argname) != 1 || Commands[argname] == nullptr ) // command not registered
				continue;

			Commands[argname]->Run({ argname, argvalue });
		}
	}

	Terminal.PrintLine();
}

void ElDorito::Tick(const std::chrono::duration<double>& DeltaTime)
{
	if( _kbhit() )
	{
		Terminal.HandleInput(_getch());
		Terminal.PrintLine();
	}
	for( auto Command : Commands )
	{
		if( Command.second )
		{
			Command.second->Tick(DeltaTime);
		}
	}
}

namespace
{
	static void HandleFinder()
	{
	};
}

std::string ElDorito::GetDirectory()
{
	char Path[MAX_PATH];
	HMODULE hMod;
	if( !GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, (LPCSTR)&::HandleFinder, &hMod) )
	{
		int Error = GetLastError();
		std::cout << "Unable to resolve current directory, error code: " << Error << std::endl;
	}
	GetModuleFileNameA(hMod, Path, sizeof(Path));
	std::string Dir(Path);
	Dir = Dir.substr(0, std::string(Dir).find_last_of('\\') + 1);
	return Dir;
}

void ElDorito::SetSessionMessage(const std::string& Message)
{
	static wchar_t msgBuf[256];
	wmemset(msgBuf, 0, 256);

	std::wstring msgUnicode = Utils::String::WidenString(Message);
	wcscpy_s(msgBuf, 256, msgUnicode.c_str());

	Pointer::Base(0x2E5338).Write<uint8_t>(0x68);
	Pointer::Base(0x2E5339).Write(&msgBuf);
	Pointer::Base(0x2E533D).Write<uint8_t>(0x90);
	Pointer::Base(0x2E533E).Write<uint8_t>(0x90);

	// todo: some way of undoing this
}

Pointer ElDorito::GetMainTls(size_t Offset)
{
	static Pointer ThreadLocalStorage;
	if( !ThreadLocalStorage && GetMainThreadID() )
	{
		size_t MainThreadID = GetMainThreadID();

		HANDLE MainThreadHandle = OpenThread(THREAD_GET_CONTEXT | THREAD_SUSPEND_RESUME | THREAD_QUERY_INFORMATION, false, MainThreadID);

		// Get thread context
		CONTEXT MainThreadContext;
		MainThreadContext.ContextFlags = CONTEXT_FULL;

		SuspendThread(MainThreadHandle);
		BOOL success = GetThreadContext(MainThreadHandle, &MainThreadContext);
		if( !success )
		{
			std::cout << "Error getting thread context: " << GetLastError() << std::endl;
			std::exit(1);
		}
		ResumeThread(MainThreadHandle);

		// Get thread selector

		LDT_ENTRY MainThreadLdt;

		success = GetThreadSelectorEntry(MainThreadHandle, MainThreadContext.SegFs, &MainThreadLdt);
		if( !success )
		{
			std::cout << "Error getting thread context: " << GetLastError() << std::endl;
		}
		size_t TlsPtrArrayAddress = (size_t)((size_t)(MainThreadLdt.HighWord.Bits.BaseHi << 24) | (MainThreadLdt.HighWord.Bits.BaseMid << 16) | MainThreadLdt.BaseLow) + 0x2C;
		size_t TlsPtrAddress = Pointer(TlsPtrArrayAddress).Read<uint32_t>();

		// Index has been consistantly 0. Keep a look out.
		ThreadLocalStorage = Pointer(TlsPtrAddress)[0];
	}

	return ThreadLocalStorage(Offset);
}