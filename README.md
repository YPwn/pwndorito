# ElDorito
![Dorite](http://media.tumblr.com/443e39ef17d62ccbc6e0f7a74c8fa431/tumblr_inline_neor2pzD3j1qb9x1g.gif)

ElDorito is a dll-hook for the russian release of Halo Online to enable features such as loading levels, disabling hud, and much more.
Simply put the `iphlpapi.dll` file into your Halo Online directory so that it is next to `eldorado.exe` and run the game like usual!
A console window should start up parallel to the game where you may input commands. Input the `help` command to get started.